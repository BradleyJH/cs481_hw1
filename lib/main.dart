/*
Programmer: Bradley Harrison
Homework: 1 'Hello World"
Due 9/10/2020
CS 481
*/
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context)  {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body:Center(
          child:Text('''
                        Hello World
                        Bradley Harrison 
                        Fall 2021  
                        Change your thoughts Change your world'''),
        ),
      ),
    );
  }
}
